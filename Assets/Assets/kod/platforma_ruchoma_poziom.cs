﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platforma_ruchoma_poziom : MonoBehaviour {

	


    // Use this for initialization
    private Vector2 startingPosition;

    public float moveLength;
    public float moveSpeed;

    private Vector3 velocity;

    private int moveDirection;
    private Rigidbody2D player;

    // Use this for initialization
    void Start()
    {
        startingPosition = new Vector2(transform.position.x, transform.position.y);
        moveDirection = 1;
        player = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x >= startingPosition.x + moveLength)
        {
            moveDirection = -1;
        }
        else if (transform.position.x <= startingPosition.x)
        {
            moveDirection = 1;
        }

        velocity = new Vector3(moveSpeed * moveDirection,0f);
        transform.position += (velocity * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            collider.transform.SetParent(transform);
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            collider.transform.SetParent(null);
        }
    }
}


