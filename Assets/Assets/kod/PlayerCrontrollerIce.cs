﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCrontrollerIce : MonoBehaviour {

	public float moveSpeed;
	private Rigidbody2D myRigidbody;
	public float jumpSpeed;

	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	public bool isGrouded;

    private bool isKilled;
    private Vector2 respawnPosition;
    private float deathTime;

    //private Animator myAnim;

    // Use this for initialization
    void Start () {

		myRigidbody = GetComponent<Rigidbody2D> ();
        //myAnim = GetComponent<Animator> ();
        respawnPosition = myRigidbody.position;
        isKilled = false;

    }
	
	// Update is called once per frame
	void Update () {

		isGrouded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
        if (!isKilled)
        {
            if (Input.GetAxisRaw("Horizontal") > 0f)
            {
                myRigidbody.velocity = new Vector3(moveSpeed, myRigidbody.velocity.y, 0f);
                transform.localScale = new Vector3(1f, 1f, 1f);
            }
            else if (Input.GetAxisRaw("Horizontal") < 0f)
            {

                myRigidbody.velocity = new Vector3(-moveSpeed, myRigidbody.velocity.y, 0f);
                transform.localScale = new Vector3(-1f, 1f, 1f);

            }
            else
                myRigidbody.velocity = new Vector3(0f, myRigidbody.velocity.y, 0f);

            if (Input.GetButtonDown("Jump"))
            {
                myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, jumpSpeed, 0f);
            }
        }

        if (isKilled && Time.time - deathTime >= 0.5f)
        {
            Respawn();
        }

        //	myAnim.SetFloat ("speed", Mathf.Abs (myRigidbody.velocity.x));
        //	myAnim.SetBool ("grounded", isGrouded); 

    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Damage")
        {
            Die();
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Damage")
        {
            Die();
        }
        else if (collider.tag == "SaveFlag")
        {
            respawnPosition = new Vector2(collider.transform.position.x, collider.transform.position.y);
        }
    }

    private void Die()
    {
        isKilled = true;
        deathTime = Time.time;
        myRigidbody.velocity = myRigidbody.velocity * 0;
    }

    private void Respawn()
    {
        myRigidbody.position = respawnPosition;
        isKilled = false;
    }
}
