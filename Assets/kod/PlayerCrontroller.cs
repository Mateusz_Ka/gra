﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCrontroller : MonoBehaviour {

	public float moveSpeed;
	private Rigidbody2D myRigidbody;
	public float jumpSpeed;

	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	public bool isGrouded;
    public int jumpCounter;

    private bool isKilled;
    private Vector2 respawnPosition;
    private float deathTime;
    private bool isFalling;

	void Start () {
        jumpCounter = 0;
		myRigidbody = GetComponent<Rigidbody2D> ();
        respawnPosition = myRigidbody.position;
        isKilled = false;
	}

	void Update () {
        isGrouded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        if (isGrouded)
        {
            jumpCounter = 0;
        }

        if (!isKilled)
        {
            if (Input.GetAxisRaw("Horizontal") > 0f)
            {
                myRigidbody.velocity = new Vector3(moveSpeed, myRigidbody.velocity.y, 0f);
            }
            else if (Input.GetAxisRaw("Horizontal") < 0f)
            {
                myRigidbody.velocity = new Vector3(-moveSpeed, myRigidbody.velocity.y, 0f);
            }
            else
                myRigidbody.velocity = new Vector3(0f, myRigidbody.velocity.y, 0f);

            if (Input.GetButtonDown("Jump"))
            {
                if (jumpCounter < 2)
                {
                    myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, jumpSpeed, 0f);
                    jumpCounter++;
                }

            }
        }

        if (isKilled && Time.time - deathTime >= 0.5f)
        {
                Respawn();
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Damage")
        {
            Die();
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Damage")
        {
            Die();
        } else if (collider.tag == "SaveFlag")
        {
            respawnPosition = new Vector2(collider.transform.position.x, collider.transform.position.y);
        }
    }

    private void Die()
    {
        isKilled = true;
        deathTime = Time.time;
        myRigidbody.velocity = myRigidbody.velocity * 0;
    }

    private void Respawn()
    { 
        myRigidbody.position = respawnPosition;
        isKilled = false;
        jumpCounter = 0;
    }
}
