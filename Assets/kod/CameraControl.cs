﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	public GameObject target;
	public float followAhead;
	public float smoothing;
	private Vector3 targetPosition;
    private float z;

	// Use this for initialization
	void Start () {
        z = transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {

		targetPosition = new Vector3(target.transform.position.x, target.transform.position.y, z);
        transform.position = Vector3.Lerp(
            transform.position,
            new Vector3(target.transform.position.x, target.transform.position.y, z),
            Time.deltaTime
        );
	}
}
