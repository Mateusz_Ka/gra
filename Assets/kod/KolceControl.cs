﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KolceControl : MonoBehaviour {

    private Vector2 startingPosition;

    public float moveLength;
    public float moveDownSpeed;
    public float moveUpSpeed;

    private Vector3 velocity;

    private int moveDirection;
    private Rigidbody2D player;
    private float moveSpeed;

    // Use this for initialization
    void Start()
    {
        startingPosition = new Vector2(transform.position.x, transform.position.y);
        moveDirection = -1;
        player = null;
        moveSpeed = moveDownSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y <= startingPosition.y - moveLength)
        {
            moveDirection = 1;
            moveSpeed = moveUpSpeed;
        }
        else if (transform.position.y >= startingPosition.y)
        {
            moveDirection = -1;
            moveSpeed = moveDownSpeed;
        }

        velocity = new Vector3(0f, moveSpeed * moveDirection);
        transform.position += (velocity * Time.deltaTime);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        moveDirection = 1;
        moveSpeed = moveUpSpeed;
        velocity = new Vector3(0f, moveSpeed * moveDirection);
        transform.position += (velocity * Time.deltaTime);

    }
}
