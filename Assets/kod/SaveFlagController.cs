﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveFlagController : MonoBehaviour {

    private SpriteRenderer sp;
    public Sprite replaceSprite;

	// Use this for initialization
	void Start () {
        sp = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            sp.sprite = replaceSprite;
        }
    }
}
