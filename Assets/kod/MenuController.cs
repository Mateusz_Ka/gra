﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public Button LavaButton, SantaButton, UnderWaterButton, IceButton;

    // Use this for initialization
    void Start () {
        LavaButton.onClick.AddListener(LoadLavaScene);
        SantaButton.onClick.AddListener(LoadSantaScene);
        UnderWaterButton.onClick.AddListener(LoadUnderWaterScene);
        IceButton.onClick.AddListener(LoadIceScene);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadLavaScene()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void LoadSantaScene()
    {
        SceneManager.LoadScene("poziom");
    }

    public void LoadUnderWaterScene()
    {
        SceneManager.LoadScene("nowa scena");
    }

    public void LoadIceScene()
    {
        SceneManager.LoadScene("SampleSceneIce");
    }
}
